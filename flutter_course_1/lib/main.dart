import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import './pages/auth.dart';
import './pages/products_home.dart';
import './pages/manage_products.dart';

void main() {
  debugPaintSizeEnabled = false;
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          //brightness: Brightness.dark,
          primarySwatch: Colors.lightGreen,
          accentColor: Colors.pinkAccent),
      //home: AuthPage(),
      routes: {
        '/': (BuildContext context) {
          return ProductsHome();
        },
        '/admin': (BuildContext context) {
          return ManageProducts();
        }
      },
    );
  }
}

class MyAppStateless extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('Header'),
          ),
          body: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(10),
                child: RaisedButton(
                  onPressed: () {
                    debugPrint('Clicked');
                  },
                  child: Text('Product'),
                ),
              ),
              Card(
                child: Column(
                  children: <Widget>[
                    Image.asset('assets/blanka.jpg'),
                    Text('Blanka')
                  ],
                ),
              ),
              Card(
                child: Column(
                  children: <Widget>[
                    Image.asset('assets/ryu.jpg'),
                    Text('Ryu')
                  ],
                ),
              ),
            ],
          )),
    );
  }
}
