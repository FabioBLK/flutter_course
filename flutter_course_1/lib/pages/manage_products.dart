import 'package:flutter/material.dart';

import 'products_home.dart';
import 'product_create.dart';
import 'product_list.dart';

class ManageProducts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
            drawer: Drawer(
              child: Column(
                children: <Widget>[
                  AppBar(
                    automaticallyImplyLeading: false,
                    title: Text('Manage Here'),
                  ),
                  ListTile(
                    title: Text('Products Menu'),
                    onTap: () {
                      //Navigator.pushNamed(context, '/');
                      Navigator.pop(context);
                      Navigator.pop(context);
                      Navigator.pop(context);
                    },
                  )
                ],
              ),
            ),
            appBar: AppBar(
              title: Text('Manage Products'),
              bottom: TabBar(tabs: <Widget>[
                Tab(
                  icon: Icon(Icons.accessibility_new),
                  text: 'Create Product',
                ),
                Tab(
                  icon: Icon(Icons.build),
                  text: 'My Products',
                )
              ]),
            ),
            body: TabBarView(
              children: <Widget>[
                ProductCreatePage(),
                ProductListPage()
              ],
            )
        )
    );
  }
}
