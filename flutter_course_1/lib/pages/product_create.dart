import 'package:flutter/material.dart';

class ProductCreatePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ProductCreatePageState();
  }
}

class _ProductCreatePageState extends State<ProductCreatePage> {
  String _titleData = '';
  String _descriptionData = '';
  double _inputPriceData = 0.0;

  _createButtonPress(BuildContext context, String title, String description, double price) {

    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Center(
            child: Text('This is a modal'),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
            margin: EdgeInsets.all(10),
            child: ListView(
              children: <Widget>[
                TextField(
                  decoration: InputDecoration(labelText: 'Title'),
                  onChanged: (String value) {
                    setState(() {
                      _titleData = value;
                    });
                  },
                ),
                TextField(
                  maxLines: 4,
                  decoration: InputDecoration(labelText: 'Description'),
                  onChanged: (String value) {
                    setState(() {
                      _descriptionData = value;
                    });
                  },
                ),
                TextField(
                  decoration: InputDecoration(labelText: 'Price'),
                  keyboardType: TextInputType.number,
                  onChanged: (String value) {
                    setState(() {
                      _inputPriceData = double.parse(value);
                    });
                  },
                ),
                Center(child: Text(_titleData),),
                RaisedButton(
                    child: Text('Save'),
                    onPressed: () => _createButtonPress(context, _titleData, _descriptionData, _inputPriceData)),
              ],
            )));
  }
}