import 'package:flutter/material.dart';

import '../product_manager.dart';
import '../pages/manage_products.dart';

class ProductsHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Drawer(
          child: Column(
            children: <Widget>[
              AppBar(
                automaticallyImplyLeading: false,
                title: Text('Choose'),
              ),
              ListTile(
                title: Text('Manage Products'),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (BuildContext context) {
                    return ManageProducts();
                  }));
                },
              )
            ],
          ),
        ),
        appBar: AppBar(
          title: Text('Header'),
        ),
        body: ProductManager([]));
  }
}
