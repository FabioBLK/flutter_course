import 'package:flutter/material.dart';

import '../pages/products_home.dart';

class AuthPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text('Login'),
        ),
        body: Center(
          child: RaisedButton(child: Text('Login'), onPressed: () {
            print('login');
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) {
              return ProductsHome();
            }));
          }),
        ));
  }
}