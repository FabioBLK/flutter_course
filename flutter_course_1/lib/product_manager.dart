import 'package:flutter/material.dart';

import './products.dart';
import './product_control.dart';

class ProductManager extends StatefulWidget {

  final List<Map<String, String>> _products;

  ProductManager(this._products) {
    print('[PRODUCT MANAGER] Constructor');
  }

  @override
  State<StatefulWidget> createState() {
    print('[PRODUCT MANAGER] Create State');
    return _ProductManagerState();
  }
}

class _ProductManagerState extends State<ProductManager> with AutomaticKeepAliveClientMixin {
  List<Map<String, String>> _products = [];
  //_ProductManagerState(this._products);
  _ProductManagerState(){
    print('[PRODUCT MANAGER STATE] Constructor');
  }

  ///*
  @override
  void initState(){
    print('[PRODUCT MANAGER STATE] initState');
    super.initState();
    _products = widget._products;
  }
  //*/

  void addProduct(Map<String, String> product) {
    setState(() {
      _products.add(product);
    });
  }

  void _addDelete(int index) {
    setState(() {
      _products.removeAt(index);
    });
  }

  @override
  Widget build(BuildContext context) {
    print('[PRODUCT MANAGER STATE] Build');
    return Column(
      children: [
        Container(
          margin: EdgeInsets.all(10),
          child: ProductControl(addProduct),
        ),
        Container(height: 300.0, child: Products(_products, _addDelete))
      ],
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
