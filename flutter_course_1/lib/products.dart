import 'package:flutter/material.dart';

import './pages/product.dart';

class Products extends StatelessWidget {
  final List<Map<String, String>> products;
  final Function deleteProduct;

  //Constructor :-O
  Products(this.products, this.deleteProduct) {
    print('[PRODUCTS] Constructor');
  }

  Card myCard(String element) {
    return Card(
      child: Column(
        children: <Widget>[Text(element)],
      ),
    );
  }

  Widget _buildProductItem(BuildContext context, int index) {
    return Card(
      child: Column(
        children: <Widget>[
          Image.asset(products[index]['image']),
          Text(products[index]['title']),
          ButtonBar(
            alignment: MainAxisAlignment.center,
            children: <Widget>[
              FlatButton(
                  child: Text('Details'),
                  onPressed: () {
                    Navigator.push<bool>(context, MaterialPageRoute(builder: (BuildContext context) {
                      return ProductPage(products[index]['title'], products[index]['image']);
                    })).then((bool value) {
                      if(value){
                        print('Deleting index ' + index.toString());
                        deleteProduct(index);
                      }
                    });
                  })
            ],
          )
        ],
      ),
    );
  }

  Widget _centeredText(String text) {
    return Center(
      child: Text(text),
    );
  }

  @override
  Widget build(BuildContext context) {
    print('[PRODUCTS] Build');
    if (products.length > 0) {
      return ListView.builder(
        itemBuilder: _buildProductItem,
        itemCount: products.length,
        //children: products.map((element) => myCard(element)).toList(),
      );
    } else {
      return _centeredText('Nothing to see here...');
    }
  }
}
