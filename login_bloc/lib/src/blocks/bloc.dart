import 'dart:async';
import 'validators.dart';
import 'package:rxdart/rxdart.dart';

class Bloc with Validators{
  final _email = BehaviorSubject<String>();
  final _password = BehaviorSubject<String>();

  Stream<String> getEmail() => _email.stream.transform(validateEmail);

  Stream<String> getPassword() => _password.stream.transform(validatePassword);

  Stream<bool> submitValid() => Observable.combineLatest2(getEmail(), getPassword(), checkFormIsValid);

  bool checkFormIsValid(String email, String password) {
    return true;
  }

  void submit() {
    print(_email.value);
    print(_password.value);
  }

  void setEmail(String email) {
    print(email);
    _email.sink.add(email);
  }

  void setPassword(String passwd) {
    _password.sink.add(passwd);
  }

  void dispose() {
    _email.close();
    _password.close();
  }
}