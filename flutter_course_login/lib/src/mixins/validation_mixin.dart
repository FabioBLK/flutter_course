class ValidationMixin {
  String validateEmailField(String s) {
    if (s.contains('@')) {
      return null;
    }
    return 'This is not a valid e-mail address';
  }

  String validatePassword(String s) {
    if (s.length >= 4) {
      return null;
    }
    return 'This is not a valid password';
  }
}