import 'package:flutter/material.dart';
import '../mixins/validation_mixin.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> with ValidationMixin {
  final formkey = GlobalKey<FormState>();
  String emailData = '';
  String passwordData = '';

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Form(
        key: formkey,
        child: Column(
          children: <Widget>[
            emailField(),
            passwordField(),
            Container(margin: EdgeInsets.only(top: 25.0),),
            submitButton()
          ],
        ),
      ),
    );
  }

  void sendInputData() {
    print('Oi');
    bool validate = formkey.currentState.validate();
    print(validate);
    if(validate) {
      formkey.currentState.save();
      print ("Email is " + emailData);
      print ("Password is " + passwordData);
    }
  }

  Widget emailField() {
    return TextFormField(
      onSaved: saveEmailField,
      validator: validateEmailField,
      decoration: InputDecoration(
          labelText: "E-mail Address",
          helperText: "you@example.com",
          hintText: "Digita aqui campeão"),
      keyboardType: TextInputType.emailAddress,
    );
  }

  Widget passwordField() {
    return TextFormField(
      onSaved: savePasswordField,
      validator: validatePassword,
      obscureText: true,
      decoration: InputDecoration(
          labelText: "Password", hintText: "Evite 123456"),
    );
  }

  Widget submitButton() {
    return RaisedButton(
      color: Colors.blue,
      child: Text("Submit"),
      onPressed: () {
        sendInputData();
      },
    );
  }

  String saveEmailField(String s) {
    emailData = s;
    return null;
  }

  String savePasswordField(String s) {
    passwordData = s;
    return null;
  }
}
