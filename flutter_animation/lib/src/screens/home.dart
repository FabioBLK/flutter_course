import 'package:flutter/material.dart';
import '../Widgets/cat.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeState();
  }
}

class HomeState extends State<Home> with TickerProviderStateMixin {
  Animation<double> catAnimation;
  AnimationController catController;

  @override
  void initState() {
    super.initState();

    catController = AnimationController(
      duration: Duration(seconds: 2),
      vsync: this
    );

    catAnimation = Tween(begin: 0.0, end: 100.0)
      .animate(
        CurvedAnimation(parent: catController, curve: Curves.easeIn)
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Animation'),),
      body: buildAnimation(),
    );
  }

  Widget buildAnimation() {
    return Cat();
  }
}