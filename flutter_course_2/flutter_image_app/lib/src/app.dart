import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' show get;

import './models/image_model.dart';
import './models/tv_show.dart';
import './widgets/image_list.dart';

class App extends StatefulWidget {
  App() {
    print("CLASS APP - Created");
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return AppState();
  }
}

class AppState extends State<App> {
  int _counter = 0;
  List<Show> _seriesData = List<Show>();

  void floatingButtonAction() async {
    print("Button Pressed - OK");

    _counter++;
    String url = 'https://jsonplaceholder.typicode.com/photos/$_counter';
    Map<String, String> headers = {'': ''};

    var response = await get(url);
    ImageModel imageModel = ImageModel.fromJson(json.decode(response.body));

    print(imageModel.title);
  }

  void tvFloatingButtonAction() async {
    print("TV Button Pressed - OK");

    List<String> series = List<String>();
    series.add('itcrowd');
    series.add('seinfeld');
    series.add('scrubs');
    series.add('friends');
    series.add('two-and-a-half-man');

    String currentSerie = series[_counter];

    String url = 'http://api.tvmaze.com/search/shows?q=$currentSerie';

    var response = await get(url);
    String body = response.body;
    String editedResponse = '{"payload" : $body}';

    var result = TvShow.fromJson(json.decode(editedResponse));

    print(result.payload[0].show.name);

    _counter++;
    if (_counter > series.length - 1) {
      _counter = 0;
    }

    setState(() {
      _seriesData.add(result.payload[0].show);
    });
  }

  @override
  Widget build(BuildContext context) {
    return mainApp();
  }

  AppState() {
    print("State APP - Created");
  }

  Widget mainApp() {
    return MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            title: Text('Lets see some images'),
          ),
          floatingActionButton: FloatingActionButton(
              onPressed: tvFloatingButtonAction, child: Icon(Icons.add)),
          body: ImageList(_seriesData)
        )
    );
  }
}
