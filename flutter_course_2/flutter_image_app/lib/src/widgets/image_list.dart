import 'package:flutter/material.dart';
import '../models/tv_show.dart' as TVShow;
import '../models/image_model.dart';

class ImageList extends StatelessWidget {
  final List<TVShow.Show> _showList;

  ImageList(this._showList) {
    print('Image List Constructor');
  }

  Widget builtItem(BuildContext context, int index) {
    return Container(
        margin: EdgeInsets.all(20.0),
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
            border: Border.all(color: Colors.black),
            color: Colors.black12,
            shape: BoxShape.rectangle),
        child: itemContent(_showList[index]));
    //Center(child: Image.network(_showList[index].image.original),)
  }

  Widget itemContent(TVShow.Show show) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(bottom: 10.0),
          child: Image.network(show.image.original),
        ),
        Center(
            child: Text(
          show.name,
          style: TextStyle(fontSize: 25.0),
        ))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: _showList.length,
      itemBuilder: builtItem,
    );
  }
}
